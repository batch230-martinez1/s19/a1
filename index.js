let username;
let password;
let role;

function login(){
    let enteredUsername = prompt("Enter your username:");
    let enteredPassword = prompt("Enter your password:");
    let enteredRole = prompt("Enter your role:");

    if(enteredUsername === "" || enteredUsername === null || enteredPassword === "" || enteredPassword === null || enteredRole === "" || enteredRole === null){
        alert("Input should not be empty.");
    }else{
        username = enteredUsername;
        password = enteredPassword;
        role = enteredRole;

        switch(role){
            case "admin":
                alert("Welcome back to the class portal, admin!");
                break;
            case "teacher":
                alert("Thank you for logging in, teacher!");
                break;
            case "student":
                alert("Welcome to the class portal, student!");
                break;
            default:
                alert("Role out of range.");
                break;
        }
    }
}


login();




function checkAverage(grade1, grade2, grade3, grade4) {

    let average = (grade1 + grade2 + grade3 + grade4) / 4;
    average = Math.round(average);
    console.log(average);
    if (average <= 74) {
        console.log("Hello, student, your average is: " + average + ". The letter equivalent is F");
    } else if (average >= 75 && average <= 79) {
        console.log("Hello, student, your average is: " + average + ". The letter equivalent is D");
    } else if (average >= 80 && average <= 84) {
        console.log("Hello, student, your average is: " + average + ". The letter equivalent is C");
    } else if (average >= 85 && average <= 89) {
        console.log("Hello, student, your average is: " + average + ". The letter equivalent is B");
    } else if (average >= 90 && average <= 95) {
        console.log("Hello, student, your average is: " + average + ". The letter equivalent is A");
    } else if (average > 96) {
        console.log("Hello, student, your average is: " + average+ ". The letter equivalent is A+");
    }
}
// Stretch goal    
    if (role === "teacher" || role === "admin" || role === undefined || role === null) {
        alert(role + "You are not allowed to access this feature!");
    }
    else{
        alert("continue to evaluate the letter equivalent of the student's average.")
        
    checkAverage(70,71,73,74); 
    checkAverage(75,75,76,78); 
    checkAverage(80,81,82,78); 
    checkAverage(84,85,78,88); 
    checkAverage(89,90,91,90); 
    checkAverage(91,96,97,95);
    }



